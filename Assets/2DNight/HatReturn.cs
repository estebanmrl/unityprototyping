using UnityEngine;
using System.Collections;

public class HatReturn : MonoBehaviour 
{
	public float hatOriginalX;
	public float hatOriginalY;
	public string hatState = "onOrigin"; //"onOrigin"; "dropped"

	IEnumerator ResetHat()
	{
		if (hatState == "dropped")
		{
			yield return new WaitForSeconds(5f);
			hatState = "onOrigin";
			gameObject.transform.position = new Vector2(hatOriginalX,hatOriginalY);
			gameObject.rigidbody2D.isKinematic = false;
		}
		yield return null;
	}

	void Update()
	{
		StartCoroutine("ResetHat");
		Debug.Log (gameObject.name);
		Debug.Log (hatState);
	}
}