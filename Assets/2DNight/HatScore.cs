﻿using UnityEngine;
using System.Collections;

public static class HatScore
{
	public static int hatCount = 0;
	public static int totalHatCount = 1; //Segun el Map
	public static float hatRoomX = 1; //Segun el Map
	public static float hatRoomY = -5; //Segun el Map

	//currentHat data
	public static bool hasHat = false;
	public static bool currentHatMove = false;
	public static GameObject currentHat;
	public static float currentHatOriginalX;
	public static float currentHatOriginalY;
	//public static GameObject player;
}
