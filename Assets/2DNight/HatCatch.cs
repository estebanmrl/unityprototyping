using UnityEngine;
using System.Collections;

public class HatCatch : MonoBehaviour 
{

	//Hats on
	float yOffset = 1.0f;
	///bool moveHat = false;

	public GameObject thisHat;
	public GameObject player;
	//Vector2 playerTransform;
	//Vector2 currentHatTransform;

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.name == "link")//"Player" in final version
		{
			//if (HatScore.hasHat == true && thisHat != HatScore.currentHat )
			//{
				//HatScore.currentHat.transform.position = new Vector2 (HatScore.currentHatOriginalX, HatScore.currentHatOriginalY);
			//}
			if (HatScore.currentHat)
			{
				if (thisHat != HatScore.currentHat)
				{
					HatScore.currentHat.GetComponent<HatReturn>().hatState = "dropped";
				}
			}

			HatScore.hasHat = true;

			//hatDrop data
			thisHat.rigidbody2D.isKinematic = false;
			
			//thisHat data
			HatScore.currentHat = thisHat;
			HatScore.currentHatOriginalX = thisHat.transform.position.x;
			HatScore.currentHatOriginalY = thisHat.transform.position.y;
			
			HatScore.currentHatMove = true; //Disable on DEATH script
			//thisHat.SetActive(false);
		}
	}

	

	void LateUpdate()
	{
		if (HatScore.currentHatMove == true)
		{
			HatScore.currentHat.transform.position = new Vector2 (player.transform.position.x, player.transform.position.y + yOffset);
		}
	}
}