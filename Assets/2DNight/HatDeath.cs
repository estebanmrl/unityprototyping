using UnityEngine;
using System.Collections;

public class HatDeath : MonoBehaviour 
{
	public GameObject playerToDie;
	GameObject hatDeath;

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.name == "link")//"Player" in final ver
		{
			if (HatScore.hasHat == true)
			{
				HatScore.hasHat = false;
				HatScore.currentHatMove = false;

				HatScore.currentHat.rigidbody2D.isKinematic = false;
				HatScore.currentHat.GetComponent<HatReturn>().hatState = "dropped";
				//yield 10 seconds
				//back to saved this hat original pos
				//Anim parpadear	
			}
			else if (HatScore.hasHat == false)
			{
				//GoTo Hat Room
				playerToDie.transform.position = new Vector2 (HatScore.hatRoomX,HatScore.hatRoomY);
			}
		}
	}

}
