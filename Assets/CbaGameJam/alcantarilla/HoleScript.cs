﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HoleScript : MonoBehaviour 
{
	public GameObject wood;
	public GameObject metal;

	public Sprite woodSprite;
	public Sprite metalSprite;
	//Sprite spriteComponent = this.GetComponent<sprite>();

	/////////////////////////0TEXTO
	public string text1 = "No puedo pasar tendria que tapar el hueco";
	public string text2 = "Todavia no se puede pasar";
	public string textWoodTry = "Podria usar la madera";
	public string textWoodSucess = "Parece que no funciona la madera";
	public string textMetalTry = "Podria usar el metal";
	public string textMetalSucess = "Ahora si se puede pasar";

	bool dialog = false;
	bool doneWood = false;
	bool doneMetal = false;

	bool woodBool = false;
	GameObject itemWood;

	void OnTriggerStay2D (Collider2D other)
	{
		if (Input.GetButtonDown("Talk"))
		{
			dialog = true;

			if (doneWood == false)
			{
				GlobalGUI.text = text1;
				foreach (var itemUsr in Inventario.Usr)
				{
					if (itemUsr == wood)
					{
						GlobalGUI.text = textWoodTry;
						PutObject (wood);
						itemWood = itemUsr;
						woodBool = true;
						doneWood = true;
					}
					else 
					{GlobalGUI.text = text2;} //Por algo llega aca si tengo el metal
				}

				if (woodBool == true)
					{

						Inventario.Usr.Remove (itemWood);
					}

			}

			if (doneWood == true && doneMetal == false)
			{
				foreach (var itemUsr in Inventario.Usr)
				{
					if (itemUsr == metal)
					{
						GlobalGUI.text = textMetalTry;
						PutObject (metal);
					}
				}


			}

			if (doneMetal)
			{
				GlobalGUI.text = textMetalSucess;
			}
					
		}
	}

	void PutObject(GameObject auxItem)
	{	
		if (Input.GetButtonDown("Talk"))
		{
			
			if (auxItem == wood)
			{
				GlobalGUI.text = textWoodSucess;
				//load wood sprite
				//GetComponent(SpriteRenderer).sprite = woodSprite;
				GetComponent<SpriteRenderer>().sprite = woodSprite;
				//spriteComponent.sprite = woodSprite;

			}
			if (auxItem == metal)
			{
				GlobalGUI.text = textMetalSucess;
				//GetComponent(SpriteRenderer).sprite = metalSprite;
				GetComponent<SpriteRenderer>().sprite = metalSprite;
				//load metal sprite

				//disable component collider
				Destroy(GetComponent("BoxCollider2D"));
				Destroy(GetComponent("EdgeCollider2D"));

			}

		} 

	}

void OnTriggerExit2D(Collider2D other)
	{
		GlobalGUI.text = "";
	}

void OnGUI ()
	{
		if (dialog)
		{
			GUI.Label (new Rect(0,0,Screen.width,Screen.height), GlobalGUI.text);
		}
	}
}