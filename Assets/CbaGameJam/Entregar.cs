﻿using UnityEngine;
using System.Collections;

public class Entregar : MonoBehaviour 
{
	public GameObject itemGive;
	public string text1 = "Take this!";
	public string textDone = "Ya te di todo";
	
	bool done = false;
	bool dialog = false;

	void OnTriggerStay2D (Collider2D other)
	{
		if (Input.GetButtonDown("Talk"))
		{
			if (done == false)
			{	
				GlobalGUI.text = text1;
				Inventario.Usr.Add(itemGive);
				dialog = true;
				done = true;
			}
			else
			{
				GlobalGUI.text = textDone;
			}
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		GlobalGUI.text = "";
	}

	void OnGUI ()
	{
		if (dialog)
		{
			GUI.Label (new Rect(0,0,Screen.width,Screen.height), GlobalGUI.text);
		}
	}
}
