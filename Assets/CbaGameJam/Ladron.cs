﻿using UnityEngine;
using System.Collections;

public class Ladron : MonoBehaviour 
{
	public string textC = "Aqui hay un ladron, mejor no";
	bool dialog = false;

	void OnTriggerEnter2D (Collider2D other)
	{
		dialog = true;
		GlobalGUI.text = textC;
	}
	
	void OnTriggerExit2D(Collider2D other)
	{
		GlobalGUI.text = "";
	}

	void OnGUI()
	{
		if (dialog)
		{
			GUI.Label (new Rect(0,0,Screen.width,Screen.height), GlobalGUI.text);
		}
	}
}
