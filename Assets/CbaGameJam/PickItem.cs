﻿using UnityEngine;
using System.Collections;

public class PickItem : MonoBehaviour {

	public GameObject Pick;

	void OnTriggerEnter2D (Collider2D other)
	{
		Inventario.Usr.Add (Pick);
		Pick.SetActive (false);

	}
}
