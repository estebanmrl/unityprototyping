﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Intercambio : MonoBehaviour 
{

	public string itemCheck;
	public GameObject itemGive;
	public string textSucess = "Hola todo bien?";
	public string textFail = "Todo mal";
	public string textDone = "Gracias por todo";
	//string text;

	GameObject auxItemUsr;
	bool giveItem = false;
	bool dialog = false;
	bool done = false;


	void OnTriggerStay2D(Collider2D other)
	{
		if (Input.GetButtonDown("Talk"))
		{
			if (done == false)
			{
				//Texto
				foreach (var itemUsr in Inventario.Usr) 
				{
					if (itemCheck == itemUsr.name)
					{
						auxItemUsr = itemUsr;
						giveItem = true;
					}
				}
				dialog = true;
				
				if (giveItem) 
				{
					Inventario.Usr.Remove (auxItemUsr);
					Inventario.Usr.Add (itemGive);
					GlobalGUI.text = textSucess;
					Inventario.ForEachInvUsr ();
					done = true;
				}
				
				if (giveItem == false)
				{
					GlobalGUI.text = textFail;			
				}
			}

			else
			{
				GlobalGUI.text = textDone;
			}
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		GlobalGUI.text = "";
	}

	void OnGUI ()
	{
		//Rect(0,0,Screen.width,Screen.height), text)
		if (dialog)
		{
			GUI.Label (new Rect(0,0,Screen.width,Screen.height), GlobalGUI.text);
		}
	}
}
