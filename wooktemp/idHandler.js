//GameObject ID handler

public static identification
{
	var nextId : int;
	private auxId : int;

	function assignId()
	{
		auxId = nextId;
		nextId++;
		return auxId;
	}
}

//unity uses a unique gameobject id, is it worth to establish this system?
//At least it will grant us some power to handle the id in our other scripts and knowing what our id is without no danger or fucking up Unity's IDs.
//If this stays simplify the aux<-next assignment system :o 
//Weapon could be simplified, creating an Object per weapon is expensive one could add the weapon to the guy by a generic type

//We could have ids for the weapons types i.e.

basicSwordID = 130;

invertory[] = {x, x, 130, x, 130} //So when the character drops it we just generate the two 130 objects type.

This will generalize all the GameObject that we need to access but will allow us to establish a global way to acces everything and keep the common objects on check.

//It will be actually cheaper to just have a sprite of the character and the weapon. Than of the two. This will be problematic to create new weapons but we could just create the objects oon the hands and destroy them when they are on the inventory