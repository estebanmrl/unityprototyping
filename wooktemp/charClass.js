//Generic Character class

class GenericChar
{
	var charID : int;
	public static IDGen : int;
	var name : String; //Unique or just generated id
	var energy : float; //int?
	var alliance : String //int?
	var level : int;
	var pseudoExp : int; //Ultra simplified experience system to addres the attack power problem
	var weapon : int; //Or a weapon ID or a function
	var inventory : Array[50]; //Store the items IDs (manage everything with ids, individualize objects)
								//Create globa count variable

	//Type stablish by polymorphism

	function GenericChar(){} //Does the constructor executes with polymorphism?

	function move()
	{		
	}

	function attack(){} //virtual

}

class Player : GenericChar{}

class Farmer : GenericChar{}

class Miner : GenericChar{}

class Warrior : GenericChar{}

class Mage/Archer : GenericChar{}