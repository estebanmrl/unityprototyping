﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour 
{
	float speed = 20;
	private float horizontalAxis;
	private float jumpForce;

	LayerMask groundLayer;
	bool isGrounded = false;
	float playerX;
	float playerY;
	float  circleRadius = 1;

	//Variable for FlipSprite
	Vector3 scaleX;

	void MovePlayer()
	{
		playerX = transform.position.x;
	    playerY = transform.position.y;

		if(Input.GetButtonDown("Jump"))//Unity has "Jump","Horizontal" and "Vertical" KeyDown built in
		{
			Vector2 charPosXY;
			charPosXY.x = playerX;
			charPosXY.y = playerY;
			isGrounded = Physics2D.OverlapCircle(charPosXY, circleRadius, groundLayer); //OverlapCircle(centerOfTheCircle, radius, layermask) [CircleRaycast]
			if (isGrounded)
			{
				rigidbody2D.AddForce(Vector2(0f, jumpForce),ForceMode2D.Impulse);
			}
		}

		else if (Input.GetAxis("Horizontal") != 0)
		{
			horizontalAxis = speed*Input.GetAxis("Horizontal"); //GetAxis returns 0.xxx or -0.xxx
			rigidbody2D.velocity.x = horizontalAxis;
			FlipSprite();
		}
	}

	void FlipSprite()
	{
		scaleX = transform.localScale;
		if (Input.GetAxis("Horizontal") > 0)
		{
			scaleX.x = 1;
			transform.localScale = scaleX;
		}
		else if (Input.GetAxis("Horizontal") < 0)
			{
				scaleX.x = -1;
				transform.localScale = scaleX;
			}
	}

	void Update()
	{
		MovePlayer();
	}
}